const validator = require('validator')
const fs = require('fs')

// #1 Read File with JSON
// #2 Convert JSON to JS Object
// #3 Return JS Object

const readJson = jsonFile => {
  // Read file to JSON
  // Convert JSON to JS Object
  const profileJson =  fs.readFileSync(jsonFile)
  const profileObject = JSON.parse(profileJson)
  return {profileObject}
}

module.exports = { readJson }
