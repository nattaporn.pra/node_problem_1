const jsonWriter = require('./jsonWriter')
const jsonReader = require('./jsonReader')
const { default: validator } = require('validator')

const profile = {
  name: 'Pongsiri Chatkaew',
  age: 24,
  email: 'pongsiri.cha@hotmail.com'
}

if(validator.isEmail(profile.email)) {
  jsonWriter.writeJson(profile)
  const profileObj = jsonReader.readJson('./profile.json')
  console.log(profileObj)  
}
